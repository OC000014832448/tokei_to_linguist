# Tokei to Linguist

A helper script that generates a map between the language formats used by tokei and linguist.

## Description

The "Comments in Code" check of the [`occmd`](https://gitlab.opencode.de/opencode-analyzer/occmd-public) tool operates on the set $L_{check} = L_{linguist} \cap L_{tokei}$, where $L_{linguist}$ is the set of all _programming_ languages reported by [`linguist`](https://github.com/github-linguist/linguist) and $L_{tokei}$ is the set of all languages supported by [`tokei`](https://github.com/XAMPPRocky/tokei).

This script computes this set by constructing the map $A: L_{tokei} \rightarrow L_{linguist}\cup\{\text{None}\}$ that satisfies $\text{Im}(A) \setminus \{\text{None}\} = L_{check}$.

## Usage
```
Script to generate map between language formats used by tokei and linguist

Usage: tokei_to_linguist [OPTIONS] [FILE]

Arguments:
  [FILE]  Output file

Options:
  -f, --format <FORMAT>  [default: json] [possible values: json, yaml]
  -h, --help             Print help
  -V, --version          Print version
```
