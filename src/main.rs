use anyhow::{self, bail, Context};
use clap::{Parser, ValueEnum};
use std::collections::{HashMap, HashSet};

// A map from the set of all languages reported by [`tokei`](https://github.com/XAMPPRocky/tokei)
// to the set of all _programming_ languages reported by
// [`linguist`](https://github.com/github-linguist/linguist), extended by the
// special element None.
//
// This can be used to create a common ground between the results of the
// respective tools.
struct Tokei2Linguist {
    map: HashMap<String, Option<String>>,
}

impl Tokei2Linguist {
    const URL_LINGUIST_LANGS: &str = "https://raw.githubusercontent.com/github-linguist/linguist/master/lib/linguist/languages.yml";
    const URL_TOKEI_LANGS: &str =
    "https://raw.githubusercontent.com/XAMPPRocky/tokei/master/languages.json";

    // download list of programming languages reported by linguist from GitHub
    fn get_linguist_langs() -> anyhow::Result<String> {
        Ok(reqwest::blocking::get(Self::URL_LINGUIST_LANGS)?.text()?)
    }

    // download list of programming languages reported by tokei from GitHub
    fn get_tokei_langs() -> anyhow::Result<String> {
        Ok(reqwest::blocking::get(Self::URL_TOKEI_LANGS)?.text()?)
    }

    // filter the list of all languages to keep only the  programming languages
    fn filter_linguist_plangs(
        raw: &str,
    ) -> anyhow::Result<HashMap<String, serde_yaml::Mapping>> {
        let outer_map: serde_yaml::Mapping =
            serde_yaml::from_str(raw).context("Invalid linguist yaml file")?;

        let plangs: HashMap<String, serde_yaml::Mapping> = outer_map
            .into_iter()
            .filter_map(|(key, inner_map)| {
                if inner_map.as_mapping().unwrap().iter().any(|(k, v)| {
                    k.as_str().unwrap() == "type"
                        && v.as_str().unwrap() == "programming"
                }) {
                    Some((
                        String::from(key.as_str().unwrap()).to_lowercase(),
                        inner_map.as_mapping().unwrap().to_owned(),
                    ))
                } else {
                    None
                }
            })
            .collect();

        Ok(plangs)
    }

    // construct a mapping from names and aliases to names, used to resolve aliases
    fn linguist_plangs() -> anyhow::Result<HashMap<String, String>> {
        let raw = Self::get_linguist_langs()
            .context("Unable to fetch linguist language list")?;

        let plangs = Self::filter_linguist_plangs(&raw)?;

        Ok(plangs.into_iter().flat_map(|(name, mut add_info)| {
        let mut name_and_aliases = Vec::<(String, String)>::new();

        name_and_aliases.push((name.clone(), name.clone()));

        let Some(serde_yaml::Value::Sequence(aliases)) = add_info.remove("aliases") else {
            return name_and_aliases;
        };

        name_and_aliases.extend(aliases.into_iter().filter_map(|alias| {
            let serde_yaml::Value::String(alias) = alias else {
                return None;
            };
                Some((alias.to_lowercase(), name.clone()))
        }));

        name_and_aliases
    }).collect())
    }

    // generate the list of all languages reported by tokei
    fn tokei_langs() -> anyhow::Result<HashSet<String>> {
        let raw = Self::get_tokei_langs()
            .context("Unable to fetch tokei language list")?;

        let serde_json::Value::Object(outer_map) = serde_json::from_str(&raw).context("Invalid tokei json file")? else {
        bail!("Invalid tokei language json");
    };

        let serde_json::Value::Object(inner_map) = outer_map["languages"].clone() else {
        bail!("Invalid tokei language json");
    };

        Ok(inner_map
            .into_iter()
            .map(|(k, _)| k.to_lowercase())
            .collect())
    }

    fn build_tokei2linguist_map(
        tokei: HashSet<String>,
        mut linguist: HashMap<String, String>,
    ) -> HashMap<String, Option<String>> {
        let mut map = HashMap::new();
        let mut special = HashMap::<_, _>::from([
            ("fortranlegacy", "fortran"),
            ("fortranmodern", "fortran"),
            ("visualbasic", "visual basic .net"),
            ("cppheader", "c++"),
            ("cheader", "c"),
            ("objectivecpp", "objective-c++"),
        ]);

        for tokei_name in &tokei {
            // cases where both tools use a common name
            if let Some(linguist_name) = linguist.remove(tokei_name) {
                map.insert(tokei_name.to_owned(), Some(linguist_name));
                continue;
            };

            // cases where we must manually map names
            if let Some(special_name) = special.remove(tokei_name.as_str()) {
                map.insert(
                    tokei_name.to_owned(),
                    Some(special_name.to_owned()),
                );
                continue;
            };

            map.insert(tokei_name.to_owned(), None);
        }

        map
    }

    pub fn build() -> anyhow::Result<Self> {
        let linguist = Self::linguist_plangs()
            .context("Unable to get linguist languages")?;
        let tokei =
            Self::tokei_langs().context("Unable to get tokei languages")?;

        Ok(Self {
            map: Self::build_tokei2linguist_map(tokei, linguist),
        })
    }

    pub fn write_to_file(
        &self,
        file_name: String,
        fmt: OutFormat,
    ) -> anyhow::Result<()> {
        let file = std::fs::File::options()
            .write(true)
            .truncate(true)
            .create(true)
            .open(file_name)?;

        match fmt {
            OutFormat::Json => serde_json::to_writer(file, &self.map)?,
            OutFormat::Yaml => serde_yaml::to_writer(file, &self.map)?,
        };

        Ok(())
    }

    pub fn write_to_stdout(&self, fmt: OutFormat) -> anyhow::Result<()> {
        match fmt {
            OutFormat::Json => {
                serde_json::to_writer(std::io::stdout(), &self.map)?
            }
            OutFormat::Yaml => {
                serde_yaml::to_writer(std::io::stdout(), &self.map)?
            }
        }

        Ok(())
    }
}

#[derive(Debug, ValueEnum, Clone)]
enum OutFormat {
    Json,
    Yaml,
}

impl std::fmt::Display for OutFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let format = match self {
            Self::Json => "json",
            Self::Yaml => "yaml",
        };
        write!(f, "{}", format)
    }
}

/// Script to generate map between language formats used by tokei and linguist
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// Output file
    file: Option<String>,

    #[arg(short, long, default_value_t = OutFormat::Json)]
    format: OutFormat,
}

fn main() {
    let cli = Cli::parse();

    let Ok(map) = Tokei2Linguist::build() else {
        println!("Failed to build map");
        return;
    };

    if let Err(e) = match cli.file {
        Some(file) => map.write_to_file(file, cli.format),
        None => map.write_to_stdout(cli.format),
    } {
        println!("Error: {}", e);
    };
}
